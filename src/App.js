import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Welcome from "./Welcome";
import CreatingArrays from "./components/1412/CreatingArrays";
import CreatingJSObjects from "./components/1412/CreatingJSObjects";
import Lecture3 from "./components/1412/Lecture3";
import UsingArrays from "./components/1412/UsingArrays";
import CreatingConditionalData from "./components/1412/CreatingConditionalData";
import ImportingJsonData from "./components/1412/ImportingJsonData";
import PassingMoreComplexProps from "./components/1412/PassingMoreComplexProps";
import PassingFunction from "./components/1412/PassingFunction";
import PassingChildren from "./components/1412/PassingChildren";
import ChangingParentsStateUsingProps from "./components/1412/ChangingParentsStateUsingProps";
import Parent from "./components/1412/Parent";
import CarShop from "./components/1412/CarShop";
import UsingArray from "./components/1412/UsingArray";
import SaveDataToLocalStorage from "./components/1412/SaveDataToLocalStorage";

function App() {
  let grade = 3;

  function setGrade(newGrade) {
    grade = newGrade;
  }

  return (
    <div className="App">
      <Welcome name="Aleksi" age="22" degree="Engineer" />
      <CreatingArrays />
      <CreatingJSObjects />
      <Lecture3 name="Timo" phone="0407677080" />
      <Lecture3 name="Matti" phone="0409988776" />
      <UsingArrays />
      <CreatingConditionalData />
      <ImportingJsonData />
      <PassingMoreComplexProps />
      <PassingFunction />
      <PassingChildren>
        <b>
          <u>Hello Vaasa</u>
        </b>
      </PassingChildren>
      <p>My course grade: {grade}</p>
      <ChangingParentsStateUsingProps function={setGrade} />
      <Parent />
      <CarShop />
      <UsingArray />
      <SaveDataToLocalStorage />
    </div>
  );
}

export default App;
