import React from "react";

function Welcome(props) {
  return <div>Hello {props.name}</div>;
}

export default Welcome;
