import React from "react";

function CreatingConditionalData() {
  const intArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  const strArr = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];
  //Need to create a dropdown here cannot loop in return
  const lessThanSix = intArr.map((month, key) => {
    //if the int value in intArr-table is less than 6
    if (month < 6) {
      //add from the other strArr value to the lessThanSix -variable
      return <option key={key}>{strArr[key]}</option>;
    }
  });
  return (
    <div className="App">
      <select name="month">{lessThanSix}</select>
    </div>
  );
}

export default CreatingConditionalData;
