import React from "react";
import cars from "./ca.json";

function UsingArray(props) {
  //the return function does not support for/if/while, have to do here
  const list = cars.map((car, key) => {
    return <li>{car.name}</li>;
  });
  return (
    <div>
      <p>Cars:</p>
      <ul>{list}</ul>
    </div>
  );
}

export default UsingArray;
