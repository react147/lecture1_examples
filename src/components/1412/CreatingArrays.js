import React from "react";

function CreatingArrays() {
  const intArr = [1, 2, 3];
  const strArr = ["Volvo", "Saab", "Lexus"];
  strArr.push("Tesla");
  return (
    <div className="App">
      <p>{intArr[1]}</p>
      <p>{strArr[2]}</p>
      <p>{strArr.length}</p>
      {strArr[strArr.length - 1]}
    </div>
  );
}

export default CreatingArrays;
