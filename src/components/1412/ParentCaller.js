import React from "react";

function ParentCaller(props) {
  props.function("Vaasa");
  props.function("Seinäjoki");
  return <div>This called parent</div>;
}

export default ParentCaller;
