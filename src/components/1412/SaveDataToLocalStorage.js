import React from "react";

function SaveDataToLocalStorage(props) {
  return (
    <div>
      <input id="id1" type="text"></input>
      <button onClick={saveToLocalStorage}>Save to localStorage</button>
      <div id="id2"></div>
    </div>
  );

  function saveToLocalStorage() {
    localStorage.setItem("data", document.getElementById("id1").value);
    alert("Saved");
  }
}

export default SaveDataToLocalStorage;
