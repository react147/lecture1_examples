import React from "react";
import ParentCaller from "./ParentCaller";

function PassingFunction() {
  function echo(city) {
    console.log("Hello " + city);
  }
  return <ParentCaller function={echo} />;
}

export default PassingFunction;
