import React from "react";

function CreatingJSObjects() {
  const course = {};
  course.id = "H2C2101-1";
  course.name = "Introduction to Programming (H2C)";
  course.teacher = "Timo Kankaanpää";
  const another = {
    id: "H2C2102-1",
    name: "Software engineering project",
    teacher: "Somebody",
    lessons: [
      { name: "Lesson1" },
      { name: "Lesson3" },
      { name: "Lesson3" },
      { name: "Lesson4", topic: "Testing" }
    ]
  };
  return (
    <div className="App">
      {another.lessons[3].topic}
      <p>{course.id}</p>
      <p>{course.name}</p>
      <p>{course.teacher}</p>
      <p>{course.id + " " + course.name}</p>
    </div>
  );
}

export default CreatingJSObjects;
