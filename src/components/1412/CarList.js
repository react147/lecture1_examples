import React from "react";

function CarList(props) {
  props.function({ key: 99, name: "Deere" });
  const cars = props.list.map((data, key) => {
    return <option key={key}>{data.name}</option>;
  });
  return (
    <div>
      <select>{cars}</select>
    </div>
  );
}

export default CarList;
