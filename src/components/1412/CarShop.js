import React from "react";
import cars from "./ca.json";
import CarList from "./CarList";

function CarShop(props) {
  function addCar(name) {
    cars.push(name);
  }
  return (
    <div>
      <CarList list={cars} function={addCar} />
    </div>
  );
}

export default CarShop;
