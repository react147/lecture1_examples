import React from "react";
import CarShow from "./CarShow";

function PassingMoreComplexProps() {
  const strArr = [
    { stand: ["Volvo", "Saab", "Opel"] },
    { stand: ["Tesla", "Lexus", "Ferrari"] }
  ];
  return (
    <div className="App">
      <CarShow cars={strArr} />
    </div>
  );
}

export default PassingMoreComplexProps;
