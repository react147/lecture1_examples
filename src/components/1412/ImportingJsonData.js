import React from "react";
import json from "./cars.json";

function ImportingJsonData() {
  const cars = json.map((car, key) => {
    return <option key={key}>{car.name}</option>;
  });
  return (
    <div className="App">
      <select name="brand">{cars}</select>
    </div>
  );
}

export default ImportingJsonData;
