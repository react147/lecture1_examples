import React from "react";

function UsingArrays() {
  const strArr = ["Volvo", "Saab", "Lexus", "Tesla"];
  //Need to create a dropdown here cannot loop in return
  const carOptions = strArr.map((car, key) => {
    return <option key={key}>{car}</option>;
  });
  return (
    <div className="App">
      <select name="brand">{carOptions}</select>
    </div>
  );
}

export default UsingArrays;
