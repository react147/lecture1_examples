import React, { useState } from "react";
import Child from "./Child";

function Parent(props) {
  const [grade, setGrade] = useState(3);
  function updateState(newValue) {
    setGrade(newValue);
  }
  return (
    <div>
      <p>Parent grade is : {grade}</p>
      <Child function={updateState} grade={grade} />
    </div>
  );
}

export default Parent;
