import React from "react";

function ChangingParentsStateUsingProps(props) {
  const myGrade = 4;
  props.function(myGrade);
  return <div>Grade changed to {myGrade}</div>;
}

export default ChangingParentsStateUsingProps;
