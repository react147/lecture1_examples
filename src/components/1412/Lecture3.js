import React from "react";

function Lecture3(props) {
  const nameAndPhone = props.name+props.phone;
  return (
    <div className="App">
      <table>
        <tr>
          <th>Name</th>
          <th>Phone</th>
        </tr>
        <tr>
          <td>{props.name}</td>
          <td>{props.phone}</td>
        </tr>
      </table>
    </div>
  );
}

export default Lecture3;
