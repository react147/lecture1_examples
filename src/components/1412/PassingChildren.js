import React from "react";

function PassingChildren(props) {
  return (
    <div>
      <p>props.children data: {props.children}</p>
    </div>
  );
}

export default PassingChildren;
