import React from "react";

function Child(props) {
  props.function(4);
  return <div>Updated parents value to {props.grade}</div>;
}

export default Child;
