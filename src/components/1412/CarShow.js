import React from "react";
//Loopin the array of two stands to have the cars

function CarShow(props) {
  const stands1 = props.cars[0].stand.map((data, key) => {
    return <option>{data}</option>;
  });
  const stands2 = props.cars[1].stand.map((data, key) => {
    return <option>{data}</option>;
  });
  return (
    <div>
      Stand 1<select>{stands1}</select>
      Stand 2<select>{stands2}</select>
    </div>
  );
}

export default CarShow;
